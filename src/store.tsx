/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { Dispatch, SetStateAction, createContext, useContext, useState } from 'react';

type State<T> = [T | undefined, Dispatch<SetStateAction<T | undefined>>];

export interface AppStatus {
  isReady: boolean;
}

export interface Auth {
  access_token?: string;
  token_type?: string;
  expires_in?: number;
}

interface Session {
  id: number;
  userId: number;
  customerId: number;
  CatalogId: number;
}

export interface Student {
  id: number;
  first_name: string;
  last_name: string;
  mail: string;
  phone: string;
  position: string;
  birthday: string;
  start_date: string;
  end_date: string;
  department: string;
  is_archived: boolean;
  avatar_uri: string;
  created_at: string;
  updated_at: string;
}

export interface Token {
  access_token?: string;
  token_type?: string;
  refresh_token?: string;
  expires_in?: number;
  expires_at?: number;
  id_token?: string;
  code?: string;
}


export type Students = Student[];


const defaultValue = [undefined, () => undefined];


// const ActiveSidebarLinkContext = createContext(defaultValue as State<SidebarElement>);
// const ActiveUserContext = createContext(defaultValue as State<User>);
const ActiveStudentContext = createContext(defaultValue as State<Student>);
const AppStatusContext = createContext(defaultValue as State<AppStatus>);
const AuthContext = createContext(defaultValue as State<Auth>);
const SessionContext = createContext(defaultValue as State<Session>);
const StudentsContext = createContext(defaultValue as State<Students>);
const TokenContext = createContext(defaultValue as State<Token>);





export const Provider: React.FC = ({ children }) => (
  <AuthContext.Provider value={useState()}>
    <AppStatusContext.Provider value={useState()}>
      <TokenContext.Provider value={useState()}>
        <SessionContext.Provider value={useState()}>
          <StudentsContext.Provider value={useState()}>
                      <ActiveStudentContext.Provider value={useState()}>
                                      {/* <ActiveSidebarLinkContext.Provider value={useState()}> */}
                                        {children}
                                      {/* </ActiveSidebarLinkContext.Provider> */}
                      </ActiveStudentContext.Provider>
                      </StudentsContext.Provider>
        </SessionContext.Provider>
      </TokenContext.Provider>
    </AppStatusContext.Provider>
  </AuthContext.Provider>
);

export const useAuthContext = () => useContext(AuthContext);
export const useAppStatusContext = () => useContext(AppStatusContext);
export const useTokenContext = () => useContext(TokenContext);
export const useSessionContext = () => useContext(SessionContext);
export const useStudentsContext = () => useContext(StudentsContext);
export const useActiveStudentContext = () => useContext(ActiveStudentContext);
// export const useActiveSidebarLinkContext = () => useContext(ActiveSidebarLinkContext);
