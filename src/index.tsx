import React from 'react';
import ReactDOM from 'react-dom';
import { createGlobalStyle } from 'styled-components/macro';
import { ThemeProvider } from '@mui/material/styles';
import { StylesProvider } from '@mui/styles';
import { theme } from 'common/styles/theme';
import { Provider } from './store';
import PoppinsRegularWoff2 from './common/fonts/Poppins-Regular.woff';
import PoppinsRegularWoff from './common/fonts/Poppins-Regular.woff2';
import PoppinsSemiBoldWoff2 from './common/fonts/Poppins-SemiBold.woff';
import PoppinsSemiBoldWoff from './common/fonts/Poppins-SemiBold.woff2';
import PoppinsBoldWoff2 from './common/fonts/Poppins-Bold.woff';
import PoppinsBoldWoff from './common/fonts/Poppins-Bold.woff2';
import App from './App';

const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    font-family: Poppins;
    font-weight: normal;
    text-decoration: none;
  }

  @font-face {
    font-family: Poppins;
    src: url(${PoppinsRegularWoff2}) format('woff2'),
        url(${PoppinsRegularWoff}) format('woff');
    font-weight: normal;
    font-style: normal;
    font-display: swap;
}

@font-face {
    font-family: PoppinsBold;
    src: url(${PoppinsBoldWoff2}) format('woff2'),
        url(${PoppinsBoldWoff}) format('woff');
    font-weight: bold;
    font-style: normal;
    font-display: swap;
}

@font-face {
    font-family: PoppinsSemiBold;
    src: url(${PoppinsSemiBoldWoff2}) format('woff2'),
        url(${PoppinsSemiBoldWoff}) format('woff');
    font-weight: 600;
    font-style: normal;
    font-display: swap;
}
`;

ReactDOM.render(
  <Provider>
    {/* https://material-ui.com/styles/advanced/#injectfirst :
     used to be able to override material-ui theme with css classes */}
    <StylesProvider injectFirst>
      <ThemeProvider theme={theme}>
        <React.StrictMode>
          <GlobalStyle />
          <App />
        </React.StrictMode>
      </ThemeProvider>
    </StylesProvider>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
