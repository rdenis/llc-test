import React, { useEffect } from 'react';
import { Redirect, Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import { routes } from 'common/routes/routes';
import { useStudentsContext } from 'store';
import { ResourceAPI } from 'common/helpers';
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import EnTranslation from 'common/helpers/translations/en.translation';
import frTranslation from 'common/helpers/translations/fr.translation';

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources: {
      en: {
        translation: EnTranslation,
      },
      fr: {
        translation: frTranslation,
      },
    },
    lng: 'en',
    fallbackLng: 'en',

    interpolation: {
      escapeValue: false,
    },
  });

const App: React.FC = () => {
  const [, setStudents] = useStudentsContext();

  // Fetch students.
  useEffect(() => {
    const fetchStudents = async () => {
      try {
        const res = await ResourceAPI.fetchAll('students');
        setStudents(res.data);
      } catch (e) {
        console.log(`😱 Fetch students request failed : ${e}`);
      }
    };
    fetchStudents();
  }, [setStudents]);

  return (
    <Router>
      <Switch>
        {/* <Route path="/callback" component={LoginCallBack} /> */}

        {routes.map(({ path, name, Component, isExact }, key) => (
          <Route path={path} component={Component} key={name} exact={isExact} />
        ))}
        <Route render={() => <Redirect to={{ pathname: '/students' }} />} />
      </Switch>
    </Router>
  );
};

export default App;
