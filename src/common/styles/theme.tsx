import { createTheme } from '@mui/material/styles';

export const theme = createTheme({
  palette: {
    // type: 'light',
    primary: {
      main: '#18AED3',
      light: '#408AEC',
      dark: '#083671',
    },
    secondary: {
      main: '#F34E3B',
    },
    text: {
      primary: '#616161',
      secondary: '#A4A4A4',
    },
    success: {
      main: '#0ACB8F',
    },
    info: {
      main: '#0063E5',
    },
    warning: {
      main: '#FFB833',
    },
    divider: '#616161',
  },
  typography: {
    h1: {
      fontWeight: 600,
    },
    h2: {
      fontWeight: 500,
    },
    h3: {
      fontWeight: 600,
    },
    button: {
      fontSize: 13,
    },
    fontWeightLight: 500,
    fontFamily: 'Poppins',
  },
  shape: {
    borderRadius: 4,
  },

});
