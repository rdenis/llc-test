import React from 'react';
import styled from 'styled-components/macro';

const Topbar: React.FC = ({ children }) => (
    <Container>
      <div>TopBar</div>
    </Container>
);

export default Topbar;

const Container = styled.div`
  display: flex;
  height: 6em;
  width: 100%;
  border-bottom: 1px solid #707070;
  justify-content: space-between;
  flex-direction: row-reverse;
`;
