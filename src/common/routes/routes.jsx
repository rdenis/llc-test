import React from 'react';
import {
  Students,
} from 'features';

import {
  DashboardIcon,
} from 'common/components/Sidebar/icons';

export const routes = [
  {
    name: 'Default',
    path: '/',
    Component: Students,
    icon: <DashboardIcon />,
  },
  {
    name: 'Students',
    path: '/students',
    Component: Students,
    icon: <DashboardIcon />,
    isExact: true,
  },
];
