/* eslint-disable @typescript-eslint/no-explicit-any */
import axios, { AxiosStatic } from 'axios';

type Resource = 'me' | 'students';

class ResourceAPI {
  public static axiosInstance: AxiosStatic = axios;

  public static axios = axios;

  public static instance: ResourceAPI;

  public static get Instance() {
    // eslint-disable-next-line no-return-assign
    return ResourceAPI.instance || (ResourceAPI.instance = new ResourceAPI());
  }

  public static fetchAll(resource: Resource) {
    return this.axios.get(`${process.env.REACT_APP_SERVER_URL}/${resource}`);
  }

  public static fetchById(resource: Resource, resourceId: number) {
    return this.axios.get(`${process.env.REACT_APP_SERVER_URL}/${resource}/${resourceId}`);
  }

  public static fetchByKey(resource: Resource, key: string) {
    return this.axios.get(`${process.env.REACT_APP_SERVER_URL}/${resource}/${key}`);
  }

  public static fetchByKeyWithCondition(resource: Resource, key: string, _condition?: string) {
    return this.axios.get(
      _condition
        ? `${process.env.REACT_APP_SERVER_URL}/${resource}/${key}&${_condition}`
        : `${process.env.REACT_APP_SERVER_URL}/${resource}/${key}`
    );
  }

  public static fetchByIdWithJoin(resource: Resource, resourceId: number, _join: string) {
    return this.axios.get(`${process.env.REACT_APP_SERVER_URL}/${resource}/${resourceId}?join=${_join}`);
  }

  public static fetchByIdWithCondition(resource: Resource, idType: string, id: number, _condition?: string) {
    return this.axios.get(
      _condition
        ? `${process.env.REACT_APP_SERVER_URL}/${resource}?${idType}=${id}&${_condition}`
        : `${process.env.REACT_APP_SERVER_URL}/${resource}?${idType}=${id}`
    );
  }

  public static duplicate(resource: Resource, id: number) {
    return this.axios.post(`${process.env.REACT_APP_SERVER_URL}/${resource}/${id}/duplicate`);
  }

  public static delete(resource: Resource, id: number) {
    return this.axios.delete(`${process.env.REACT_APP_SERVER_URL}/${resource}/${id}`);
  }

  public static archive(resource: Resource, body: any) {
    return this.axios.patch(`${process.env.REACT_APP_SERVER_URL}/${resource}/${body.id}`, {
      reference: body.reference,
      customer_id: body.customer_id,
      is_archived: true,
    });
  }

  public static update(resource: Resource, id: number, body: any) {
    return this.axios.put(`${process.env.REACT_APP_SERVER_URL}/${resource}/${id}`, body);
  }

  public static post(resource: Resource, body: any) {
    return this.axios.post(`${process.env.REACT_APP_SERVER_URL}/${resource}`, body);
  }
}

export default ResourceAPI;
