/* eslint-disable comma-dangle */
/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { AxiosError } from 'axios';

export const handleAxiosError = (error: AxiosError, message: string, setToken: (token: any) => void) => {
  console.error(message, error.message);

  if (error.response && error.response.status === 401) {
    sessionStorage.removeItem('token');
    setToken({});
  }
};

export const handleApiError = (error: unknown) => {
  console.log(error);
};
