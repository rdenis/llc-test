/* eslint-disable no-param-reassign */
/* eslint-disable quotes */
export const flattenObject = (obj) => {
  const flattened = {};

  Object.keys(obj).forEach((key) => {
    if (typeof obj[key] === "object" && obj[key] !== null) {
      Object.assign(flattened, flattenObject(obj[key]));
    } else {
      flattened[key] = obj[key];
    }
  });

  return flattened;
};

export function flatten(data) {
  return data.reduce((result, next) => {
    result.push(next);
    if (next.items) {
      result = result.concat(flatten(next.items));
      next.items = [];
    }
    return result;
  }, []);
}
