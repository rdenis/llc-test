Feature: Sidebar

    Scenario: See the sidebar
    Given I open the app
    Then I see the "home" page
    And I can see the "sidebar" component
    And in the "sidebar" I can see 7 items
    And in the "sidebar" the first element is a logo
    And in the "sidebar" the second element is a navigation link called "Dashboard"
    And in the "sidebar" the third element is a navigation link called "Administration"
    And in the "sidebar" the fourth element is a search bar 
    And in the "sidebar" the fifth element is a navigation link called "Monitoring"
    And in the "sidebar" the sixth element is a navigation link called "Customer"
    And in the "sidebar" the seventh element is a navigation link called "Catalogs"
    And in the "sidebar" the eighth element is a navigation link called "Orders"
    And in the "sidebar" the nineth element is a navigation link called "Management"


    Scenario: Use the sidebar
    Given I open the app
    Then I click on the first element
    And I am on the "main" page
    Then I click on the second element
    And I am on the "Dashboard" page
    Then I click on the third element   
    And I am on the "Administration" page
    Then I click on the fourth element
    And I type "CloudTemple"
    And I press "return"
    Then the customer title should be "Cloud Temple"
    And I click on the fifth element
    Then I am on the "Monitoring" page
    And I click on the fifth element
    Then I am on the "Customers" page
    And I click on the fifth element
    Then I am on the "Catalogs" page
    And I click on the fifth element
    Then I am on the "Orders" page
    And I click on the fifth element
    Then I am on the "Management" page